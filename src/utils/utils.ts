/*
 * Returns the hours of the calendar
 */
export const getHours = () => {
    const hours = []
    for (let i = 9; i <= 21; i++) {
        hours.push(`${i}:00`)
    }
    return hours
}
