export default interface CalendarEvent {
    id: number
    start: string
    duration: number
    end?: string
    width?: string
    height?: string
    top?: string
    left?: string
    position?: number
    leftMargin?: number
}
