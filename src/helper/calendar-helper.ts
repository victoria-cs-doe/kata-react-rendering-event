import inputEvents from '../data/input.json'
import CalendarEvent from '../model/CalendarEvent'
import moment from 'moment'

/*
 * Orders events by starting time
 */
export const orderEventsByStartingTime = (): Array<CalendarEvent> => {
    const events = inputEvents.sort((a, b) => (a.start < b.start ? -1 : 1))
    events.forEach((e) => calculateEventEndTime(e))
    return events
}

/*
 * Iterates over the array of events and determines the array of its overlapping events
 */
export const getMapOfOverlappingEvents = (
    eventsArray: Array<CalendarEvent>
): Map<CalendarEvent, Array<CalendarEvent>> => {
    const overlappingEventsMap = new Map<CalendarEvent, Array<CalendarEvent>>()
    eventsArray.forEach((event) => {
        overlappingEventsMap.set(event, getOverlappingEvents(event, eventsArray))
    })
    return overlappingEventsMap
}

/*
 * Determines an event's height and width parameters
 * Determines an event's position on the graph
 * If an event is 90 minutes long its height will be 90px
 * If an event has 3 overlapping events, it will take 1/4th of the total allocated space (in terms of width)
 * If an event does not have any overlapping events, it will be positioned first
 * If an event has a number n of overlapping events but is the first to start, it will also be positioned first
 */
export const determineEventsDimensions = (eventsMap: Map<CalendarEvent, Array<CalendarEvent>>) => {
    const eventsArray = Array.from(eventsMap.keys())
    for (const [key, value] of eventsMap) {
        key.left = '0'
        key.top = '0'
        key.height = `${key.duration}px`
        key.width = determineEventWidth(key, eventsArray)
        key.position = 0

        const previous = previousEvents(key, eventsMap)
        if (value.length === 0 || previous.length === 0) {
            key.position = 1
            key.leftMargin = parseInt(key.width)
        } else if (previous.length === 1) {
            key.position = previous[0].position === 1 ? 2 : 1
            key.leftMargin = previous[0].width ? parseInt(previous[0].width) : 0
        } else if (previous.length > 1) {
            let overlappingEvents = getOverlappingEvents(key, eventsArray)
            overlappingEvents.push(key)
            overlappingEvents = overlappingEvents.sort((a, b) => (a.duration < b.duration ? -1 : 1))
            overlappingEvents.forEach((e, index) => {
                e.position = index + 1
                e.leftMargin = overlappingEvents[0].width ? parseInt(overlappingEvents[0].width) : 0
            })
        }
    }
    return eventsMap
}

/*
 * Returns all useful information to update the event array hook
 * Determines position parameter to place the event on the screen
 * Determines top parameter according to the start hour of the event
 * Determines left parameter according to its width and the number of overlapping events
 */
export const determineTopLeftParameters = (
    eventsMap: Map<CalendarEvent, Array<CalendarEvent>>
): Array<CalendarEvent> => {
    const eventsArray = new Array<CalendarEvent>()

    for (const [key, value] of eventsMap) {
        const top = ((parseInt(key.start.replace(':', '')) - 900) / 170) * 100
        if (key.leftMargin && key.position) {
            if (value.length === 0 || key.position === 1) {
                key.left = '0'
            } else {
                key.left = String(key.leftMargin * (key.position - 1))
            }

            const event: CalendarEvent = {
                id: key.id,
                start: key.start,
                duration: key.duration,
                end: key.end,
                width: key.width,
                height: key.height,
                top: `${top}px`,
                left: `${key.left}px`,
                position: key.position,
                leftMargin: key.leftMargin
            }
            eventsArray.push(event)
        }
    }
    return eventsArray
}

/*
 * Calculates each event's end time
 */
const calculateEventEndTime = (event: CalendarEvent) => {
    event.end = moment(event.start, 'HH:mm').add(event.duration, 'minute').format('HH:mm')
}

/*
 * For an event e, if one other starts at the same time it will be considered as overlapping
 * For this event e, if one other starts between e's start and e's end it will also be considered as overlapping
 * If this event e starts after any other event t and before t ends, t will be considered as overlapping
 */
const getOverlappingEvents = (event: CalendarEvent, eventsArray: Array<CalendarEvent>): Array<CalendarEvent> => {
    const overlappingEvents = new Array<CalendarEvent>()
    eventsArray.forEach((e) => {
        if (e.id !== event.id) {
            if (
                e.start === event.start ||
                (event.start > e.start && event.start < (e.end || '00:00')) ||
                (e.start > event.start && e.start < (event.end || '00:00'))
            ) {
                overlappingEvents.push(e)
            }
        }
    })
    return overlappingEvents
}

/*
 * Determines if an event has the first position depending on its overlapping others
 */
const previousEvents = (event: CalendarEvent, eventsMap: Map<CalendarEvent, Array<CalendarEvent>>) => {
    const arrayOfKeys = Array.from(eventsMap.keys())
    return arrayOfKeys.filter((e) => {
        return e.id !== event.id && (e.start < event.start || e.start === event.start) && (e.end || '00:00') > event.start
    })
}

/*
 * Determines the width of an event according to its number of overlapping events and if they overlap too
 */
const determineEventWidth = (event: CalendarEvent, eventsArray: Array<CalendarEvent>) => {
    const maxWidth = window.innerWidth - 240
    const overlappingEvents = getOverlappingEvents(event, eventsArray)
    if (overlappingEvents.length <= 1) {
        return `${maxWidth / (overlappingEvents.length + 1)}px`
    } else if (overlappingEvents.length === 2) {
        const eventsOverlap = overlaps(overlappingEvents[0], overlappingEvents[1], eventsArray)
        return eventsOverlap ? `${maxWidth / 3}px` : `${maxWidth / 2}px`
    }
    return `${maxWidth / 3}px`
}

/*
 * Determines whether two events overlap or not
 */
const overlaps = (
    firstEvent: CalendarEvent,
    secondEvent: CalendarEvent,
    eventsArray: Array<CalendarEvent>
): boolean => {
    return getOverlappingEvents(firstEvent, eventsArray).includes(secondEvent)
}
