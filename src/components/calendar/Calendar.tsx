import { useEffect, useState } from 'react'
import {
    getMapOfOverlappingEvents,
    orderEventsByStartingTime,
    determineEventsDimensions,
    determineTopLeftParameters
} from '../../helper/calendar-helper'
import CalendarEvent from '../../model/CalendarEvent'
import '../../styles/calendar.css'
import { Slot } from '../slot/Slot'
import { Time } from '../time/Time'

export const Calendar = () => {
    const [events, setEvents] = useState(new Array<CalendarEvent>())

    const inputEvents = orderEventsByStartingTime()
    const mapOverlapping = getMapOfOverlappingEvents(inputEvents)
    const mapDimensions = determineEventsDimensions(mapOverlapping)
    const eventsArray = determineTopLeftParameters(mapDimensions)

    useEffect(() => {
        setEvents(eventsArray)
    }, [])

    return (
        <div>
            <Time />
            <Slot events={events} />
        </div>
    )
}
