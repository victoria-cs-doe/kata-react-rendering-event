import CalendarEvent from '../../model/CalendarEvent'
import '../../styles/slot.css'

export const Slot = ({ events }: { events: Array<CalendarEvent> }) => {
    return (
        <div className="flex justify-end">
            <div className="relative w-11/12">
                {events.map((e, index) => {
                    return (
                        <div
                            className="absolute bg-pink-900 border-stone-900 border-2 flex justify-center items-center"
                            key={index}
                            style={{ top: e.top, left: e.left, width: e.width, height: e.height }}
                        >
                            {e.id}
                        </div>
                    )
                })}
            </div>
        </div>
    )
}
