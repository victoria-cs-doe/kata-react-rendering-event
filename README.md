# kata-react-rendering-event

## Author

Victoria Doe

## Software

React v18.2.0  
TypeScript v4.7.4

## How to run the project

To run the project locally, first make sure you have Node 16 or higher and npm (and/or yarn) 8 or higher installed.  
Clone this [URL]("https://gitlab.com/victoria-cs-doe/kata-bank-account.git") using the command :

```bash
    git clone url
```

You must first install all the node_modules by executing the following command :

```bash
  npm install
```

To run the project in production mode :

```bash
  npm run start
```

You will also need to set the following environment variables in a .env file at the root of the project (do not wrap variables in quotes):  
| Environment Variable | Default Value | Type |
| -------------------- | ------------- | --------- |
| PORT | 3006 | integer |

## Requirements

The problem consists in rendering events on a calendar, avoiding overlapping events to visually overlap.  
Your implementation should meet the two following constraints:

-   Every overlapping event should have the same width as every event it overlaps
-   Every event should use the maximum width available while satisfying constraint 1

Rendering events on a calendar means here:  
The relative position of events to the top of the screen and their height is a function of the height of the screen, the start/end time of the calendar, and the start time/duration of the events.  
For example: if the calendar goes from 00:00 to 24:00 and the screen is 2400px high, an event starting at 12:00 and lasting 1h would be positioned at 1200px of the top of the screen and have a height of 100px.  
Using the maximum width available here implies that the width of every group of mutually overlapping events equals the width of the window.
